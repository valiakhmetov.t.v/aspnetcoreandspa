import React, { Component } from 'react'; // let's also import Component
import './SimpleForm.scss'

type WeatherInfo = {
  date: string,
  temperatureC: string
  temperatureF: string
  summary: string
}

interface TableState {
  rows: WeatherInfo[];
}

export const Row = ({date, temperatureC, temperatureF, summary}: WeatherInfo) => (
  <tr key={date}>
    <td>{date}</td>
    <td>{temperatureC}</td>
    <td>{temperatureF}</td>
    <td>{summary}</td>
  </tr>
);

export class SimpleForm extends  Component<{}, TableState> {
  constructor(p: {}) {
    super(p);
    this.state = {
       rows: []
    };
}
  
    
  render() {
    return (
      <div>
      <table className="table-latitude">
        <thead>
        <tr>
          <th>Date</th>
          <th>TemperatureC</th>
          <th>Summary</th>
          </tr>
        </thead>
        <tbody>
        {this.state.rows.map((row) => {
            return <Row {...row} key={row.date}/>;
        })}
        </tbody>
      </table>
      <button onClick = {()=>this.GetRowData()}>Get weather</button>
      </div>
    );
  }

  GetRowData()
  {
    fetch('http://localhost:54470/WeatherForecast').then(response=>response.json()).then(json =>
      {
        this.setState({rows:json});
      }
    );
  }
}